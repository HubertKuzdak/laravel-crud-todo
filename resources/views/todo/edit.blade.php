@extends('layouts.panel')

@section('title')
    Edycja zadania
@endsection

@section('content')

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{url('/todo')}}">Panel</a></li>
                    <li class="active">Edycja zadania</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">            
            <div class="row">
                <div class="col-lg-12">
                    @if ($errors->any())
                        <div class="alert alert-danger text-left">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif    
                    @if (Session::has('todo_action'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('todo_action')}}.<a href="{{url('/todo')}}" class="alert-link"> Klinkij tutaj aby wrócić do listy</a>
                        </div>
                    @endif                                          
                    <div class="card">
                        <div class="card-header">
                        <strong>Nowe zadanie</strong>
                        </div>
                        <div class="card-body card-block">
                            <form action="{{url('/todo/'.$todo->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}  
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="todoTitle" class=" form-control-label">Nazwa zadania</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="todoTitle" name="todoTitle" value="{{$todo->title}}" class="form-control"><small class="form-text text-muted">Nazwa nowego zadania</small></div>
                                </div>
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="todoDescription" class=" form-control-label">Opis zadania</label></div>
                                <div class="col-12 col-md-9"><textarea name="todoDescription" id="todoDescription" rows="9" class="form-control"> {{$todo->description}} </textarea></div>
                                </div>
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="deadline" class=" form-control-label">Czas na wykonanie</label></div>
                                <div class="col-12 col-md-9"><input type="date" name="deadline" id="deadline" class="form-control" value="{{Carbon\Carbon::parse($todo->deadline)->format('Y-m-d')}}" required="required" title="deadline"></div>
                                </div>                                 
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="select" class=" form-control-label">Użytkownik</label></div>
                                <div class="col-12 col-md-9">
                                @if(count($users) > 0)
                                    <select name="user" id="user" class="form-control">
                                            @foreach($users as $user)
                                                <option value="{{$todo->user_id}}">{{$todo->user->name}}</option>
                                                @if($user->id != $todo->user->id)<option value="{{$user->id}}">{{$user->name}}</option>@endif
                                            @endforeach
                                    </select>
                                @else
                                    <select name="user" id="user" class="form-control" disabled>
                                            <option>Brak użytkowników</option>
                                    </select>
                                @endif
                                </div>
                                </div>
                                <div class="form-actions form-group text-center"><button type="submit" class="btn btn-success btn">Zapisz</button></div>
                            </form>
                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
