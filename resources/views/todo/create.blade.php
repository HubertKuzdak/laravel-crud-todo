@extends('layouts.panel')

@section('title')
Nowe zadanie
@endsection

@section('content')

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{url('/todo')}}">Panel</a></li>
                    <li class="active">Nowe zadanie</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">            
            <div class="row">
                <div class="col-lg-12">
                    @if ($errors->any())
                        <div class="alert alert-danger text-left">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif    
                    @if (Session::has('todo_action'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('todo_action')}}.<a href="{{url('/todo')}}" class="alert-link"> Klinkij tutaj aby wrócić do listy</a>
                        </div>
                    @endif                                          
                    <div class="card">
                        <div class="card-header">
                        <strong>Nowe zadanie</strong>
                        </div>
                        <div class="card-body card-block">
                            <form action="{{url('/todo')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="todoTitle" class=" form-control-label">Nazwa zadania</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="todoTitle" name="todoTitle" placeholder="Nazwa zadania" class="form-control"><small class="form-text text-muted">Nazwa nowego zadania</small></div>
                                </div>
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="todoDescription" class=" form-control-label">Opis zadania</label></div>
                                <div class="col-12 col-md-9"><textarea name="todoDescription" id="todoDescription" rows="9" placeholder="Opis zadania" class="form-control"></textarea></div>
                                </div>
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="deadline" class=" form-control-label">Czas na wykonanie</label></div>
                                <div class="col-12 col-md-9"><input type="date" name="deadline" id="deadline" class="form-control" value="" required="required" title="deadline"></div>
                                </div>                                 
                                <div class="row form-group">
                                <div class="col col-md-3"><label for="select" class=" form-control-label">Użytkownik</label></div>
                                <div class="col-12 col-md-9">
                                @if(count($users) > 0)
                                    <select name="user" id="user" class="form-control">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                    </select>
                                @else
                                    <select name="user" id="user" class="form-control" disabled>
                                            <option>Brak użytkowników</option>
                                    </select>
                                @endif
                                </div>
                                </div>
                                <div class="form-actions form-group text-center"><button type="submit" class="btn btn-success btn">Dodaj zadanie</button></div>
                            </form>
                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
