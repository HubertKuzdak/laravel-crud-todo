@extends('layouts.panel')

@section('title')

Lista zadań

@endsection

@section('content')
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-check-box text-success border-success"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Wykonane zadania</div>
                                        <div class="stat-digit">{{$tasksDone}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-pencil-alt2 text-muted border-muted"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Zadania do wykonania</div>
                                        <div class="stat-digit">{{$tasksTodo}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>       
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-stats-up text-info border-info"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Wszystkich zadań</div>
                                        <div class="stat-digit">{{count($todos)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Użytkowników</div>
                                        <div class="stat-digit">{{$users}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                                 

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Lista zadań do wykonania</strong>
                        </div>
                        <div class="card-body">
                            @if(count($todos) > 0)

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Number</th>
                                            <th>Kod</th>
                                            <th>Nazwa</th>
                                            <th>Opis</th>
                                            <th>Termin</th>
                                            <th>Status</th>
                                            <th>Osoba</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($todos as $index => $todo)
                                                <tr>
                                                    <th>{{++$index}}</th>
                                                    <th>{{$todo->hash}}</th>
                                                    <th>{{$todo->title}}</th>
                                                    <th>{{$todo->description}}</th>
                                                    <th>{{$todo->dedline}}</th>
                                                    <th>
                                                        @if($todo->is_done == 0)
                                                            <span class="text-warning">W trakcie</span>
                                                        @else
                                                        <span class="text-success">Wykonane</span>
                                                        @endif
                                                    </th>
                                                    <th>{{$todo->user->name}}</th>
                                                    <th>
                                                        <form method="POST" action="{{url('/todo')}}/{{$todo->id}}">
                                                            {{ csrf_field() }}
                                                            {{ method_field('PUT') }}  
                                                            
                                                            <input type="hidden" name="endTask" class="form-control" value="true">
                                                            
                                                            <button type="submit" class="btn btn-link text-success">Zakończ</button>
                                                        </form>
                                                    </th>
                                                    <th>
                                                        <a href="{{url('todo')}}/{{$todo->id}}/edit"> <span class="text-warning btn">Edytuj </span> </a>
                                                    </th>
                                                    <th>
                                                        <form method="POST" action="{{url('/todo')}}/{{$todo->id}}">
                                                            {{csrf_field()}}
                                                            {{ method_field('DELETE') }}
                                                            <button type="submit" class="btn btn-link text-danger">Usuń</button>
                                                        </form>
                                                    </th>
                                                </tr>
                                            @endforeach                                            
                                        </tbody>
                                </table>
                            @else
                                <p class="text-center">Brak zadań</p>
                            @endif
                        </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
@endsection
