<?php

Route::get('/', "Auth\LoginController@showLoginForm");

Auth::routes();

Route::resource('todo', 'TodoController', ['except' => [
    'show'
]]);