<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'hash', 'title', 'dedline', 'is_done', 'user_id', 'description'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
