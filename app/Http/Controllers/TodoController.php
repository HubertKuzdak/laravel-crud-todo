<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Todo;
use App\User;
use Carbon\Carbon;

class TodoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Todo $todo,User $user)
    {
        $todos = $todo->all();
        $tasksDone = $todo->where('is_done', 1)->count();
        $tasksTodo = $todo->where('is_done', 0)->count();
        $users = $user->count();
        return view('todo.index',compact('todos','tasksDone','tasksTodo', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $users = $user->all();
        return view('todo.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Todo $todo)
    {
        $this->validate($request, [
            'todoTitle' => 'required',
            'todoDescription' => 'required',
            'deadline' => 'required',
            'user' => 'required',
        ],[
            'todoTitle.required' => 'Pole tytuł jest wymagane',
            'todoDescription.required' => 'Pole opis jest wymagane',
            'deadline.required' => 'Pole termin zakończenia jest wymagane',
            'user.required' => 'Użytkownik nie został wybrany'
        ]);

        $hash = $this->generateHash();
        $query  = $todo->create([
            'hash' => $hash,
            'title' => $request->todoTitle,
            'description' => "zadanie",
            'deadline' => $request->deadline,
            'is_done' => 0,
            'user_id' => $request->user,
        ]);
        $request->session()->flash('todo_action', 'Nowe zadanie zostało dodane');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo,User $user,Carbon $carbon)
    {
        $users = $user->all();
        return view('todo.edit',compact('users','todo','carbon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        if ($request->endTask) {
            $todo->update([
                'is_done' => 1
            ]);
        }
        $this->validate($request, [
            'todoTitle' => 'required',
            'todoDescription' => 'required',
            'deadline' => 'required',
            'user' => 'required',
        ],[
            'todoTitle.required' => 'Pole tytuł jest wymagane',
            'todoDescription.required' => 'Pole opis jest wymagane',
            'deadline.required' => 'Pole termin zakończenia jest wymagane',
            'user.required' => 'Użytkownik nie został wybrany'
        ]);

        $hash = $this->generateHash();
        $query  = $todo->update([
            'hash' => $hash,
            'title' => $request->todoTitle,
            'description' => "zadanie",
            'deadline' => $request->deadline,
            'is_done' => 0,
            'user_id' => $request->user,
        ]);
        $request->session()->flash('todo_action', 'Zadanie zostało edytowane');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $todo->delete();
        return back();
    }

    function generateHash($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $hashLength = strlen($characters);
        $hash = '';
        for ($i = 0; $i < $length; $i++) {
            $hash .= $characters[rand(0, $hashLength - 1)];
        }
        return $hash;
    }
}
